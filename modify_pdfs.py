import logging
import os
import pathlib
from pathlib import Path

import coloredlogs
from pypdf import PdfReader, PdfWriter
from yaml import Loader, load

coloredlogs.install()

logging.basicConfig(level=logging.DEBUG)

SCRIPT_PATH = pathlib.Path(__file__).parent.resolve()
OUTPUT_DIR = SCRIPT_PATH / "output"
INPUT_DIR = SCRIPT_PATH / "input"
CONFIG_FILE = SCRIPT_PATH / "config.yaml"


def get_files(path, ext="pdf") -> list[Path]:
    """
    Retrieves list of files that end with the extension passed
    """
    files = [path / i for i in os.listdir(path) if i.endswith(f".{ext}")]
    logging.info(f"Found in {path}: {len(files)} PDF files")
    return files


def make_pdf(
    input_filename: str,
    metadata_attributes: dict[str, str],
    output_filename: str,
):
    """
    Reads PDF, updates metadata attributes, and writes to new file
    """
    reader = PdfReader(INPUT_DIR / input_filename)
    writer = PdfWriter()

    # copy pdf content as-is
    metadata = reader.metadata
    writer.add_metadata(metadata)
    for page in reader.pages:
        writer.add_page(page)

    # set new metadata attributes
    writer.add_metadata(metadata_attributes)

    # write new file
    with open(OUTPUT_DIR / output_filename, "wb") as x:
        writer.write(x)


def make_dir(path):
    """
    Creates directory if it does not exist already
    """
    if not os.path.exists(path):
        os.makedirs(path)


def prep_file_metadata(
    common_attributes: dict[str, str],
    file_attributes_or_title: str | dict[str, dict | str],
) -> dict[str, str]:
    """
    Builds metadata attribute dictionary to pass during update
    """

    attributes = {}

    for k, v in common_attributes.items():
        attributes[f"/{k}"] = v

    if isinstance(file_attributes_or_title, str):
        attributes["/Title"] = file_attributes_or_title
    elif isinstance(file_attributes_or_title, dict):
        for k, v in file_attributes_or_title["metadata"].items():
            attributes[f"/{k}"] = v

    return attributes


def get_output_filename(
    input_filename: str,
    file_attributes_or_title: str | dict[str, dict | str],
) -> str:
    """
    Determines output filename from the config provided. If not specified, keeps input filename.
    """
    if isinstance(file_attributes_or_title, str):
        return input_filename

    name = file_attributes_or_title.get("name")
    if name is None:
        return input_filename

    assert isinstance(name, str), f"name should be a string, got {type(name)} instead"

    return name if name.endswith(".pdf") else f"{name}.pdf"


def load_config(config_file) -> dict[dict]:
    """
    Reads and validates config yaml
    """
    logging.debug(f"Searching for config_file: {config_file}")

    with open(config_file, "r") as file:
        config: dict = load(file, Loader)

    if config.get("common") is None:
        config["common"] = {}

    msg = "Setting common attributes across all files:"
    for k, v in config["common"].items():
        msg += f' {k}="{v}" '

    logging.info(msg)

    files = config.get("files")
    assert (
        files is not None
    ), "'files' key is missing from config, please refer to template file for the file structure"

    for k, v in files.items():
        if isinstance(v, str):
            msg = f'Specified: {k} => Title="{v}"'
        elif isinstance(v, dict):
            metadata = v.get("metadata")

            assert metadata is not None, f"missing 'metadata' key under {k}"

            msg = f"Specified: {k} => "
            for mk, mv in metadata.items():
                msg += f' {mk}="{mv}"'

            name = v.get("name")
            if name is not None:
                msg += f" | output => {name}"

        logging.info(msg)

    return config


if __name__ == "__main__":
    make_dir(OUTPUT_DIR)
    make_dir(INPUT_DIR)

    config = load_config(CONFIG_FILE)

    for f in get_files(INPUT_DIR):
        input_filename = f.name.replace(".pdf", "")

        file_attributes_or_title = config["files"].get(input_filename)
        if file_attributes_or_title is None:
            logging.warning(f"no attributes for: {f.name}")
            continue

        make_pdf(
            f.name,
            prep_file_metadata(
                config["common"],
                file_attributes_or_title,
            ),
            get_output_filename(f.name, file_attributes_or_title),
        )
