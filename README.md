# PDF Attribute Modifier

Simple python scripts that takes all files in an input directory, updates its metadata as specified in a configuration yaml, and saves to an output directory.

## Installation

1. Open terminal and navigate to desired directory location
1. Clone git repo via: `git clone https://gitlab.com/careduz/pdf-attribute-modifier`
1. Enter new project folder: `cd pdf-attribute-modifier`
1. Create virtualenv: `python3 -m venv .venv`
1. Activate virtualenv: `source .venv/bin/activate`
1. Copy `config.template.yaml` to `config.yaml`
1. Execute script: `python modify_pdfs.py`

## Update PDFs

1. Activate virtualenv: `source .venv/bin/activate`
1. Put PDFs in the project under the `input/` folder
1. Update `config.yaml` with desired updates, following the instructions in that file
1. Get updated PDFs in the `output/` folder

> NOTE: `input` and `output` folders are created automatically if they don't exist
